﻿using System.Linq;

namespace AdventOfCode2017
{
    public class Day2 : IDay
    {
        public string Part1(string input)
        {
            var lines = input.Split('\n');
            int checksum = 0;
            foreach (var line in lines)
            {
                var numbers = line.Split('\t').Select(p => int.Parse(p)).OrderBy(p => p);
                var smallest = numbers.First();
                var largest = numbers.Last();
                checksum += largest - smallest;
            }

            return checksum.ToString();
        }

        public string Part2(string input)
        {
            var lines = input.Split('\n');
            int checksum = 0;
            foreach (var line in lines)
            {
                var numbers = line.Split('\t').Select(p => int.Parse(p)).OrderBy(p => p);
                foreach (var number in numbers)
                {
                    var divisibleValues = numbers.Where(p => p != number && number % p == 0);
                    if (divisibleValues.Count() > 0)
                    {
                        checksum += number / divisibleValues.First();
                    }
                }
            }

            return checksum.ToString();
        }
    }
}