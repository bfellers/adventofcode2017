﻿using System;

namespace AdventOfCode2017
{
    public class Day3 : IDay
    {
        private const string DIRECTION_UP = "up";
        private const string DIRECTION_DOWN = "down";
        private const string DIRECTION_LEFT = "left";
        private const string DIRECTION_RIGHT = "right";
        private const string DIRECTION_NULL = null;

        public string Part1(string input)
        {
            var location = int.Parse(input);

            var sizeOfArray = 1;
            while (true)
            {
                if (sizeOfArray * sizeOfArray > location)
                {
                    break;
                }
                sizeOfArray += 2;
            }
            // for safety and not worrying about negative array checks later on
            sizeOfArray += 2;

            var locationArray = new int[sizeOfArray, sizeOfArray];

            int posX = (sizeOfArray - 1) / 2;
            int posY = (sizeOfArray - 1) / 2;

            var startingLoc = posX;

            string direction = DIRECTION_NULL;

            for (int i = 1; i <= location; i++)
            {
                switch (direction)
                {
                    case DIRECTION_RIGHT:
                        posY++;
                        if (locationArray[posX - 1, posY] == 0)
                        {
                            direction = DIRECTION_UP;
                        }
                        break;

                    case DIRECTION_LEFT:
                        posY--;
                        if (locationArray[posX + 1, posY] == 0)
                        {
                            direction = DIRECTION_DOWN;
                        }
                        break;

                    case DIRECTION_UP:
                        posX--;
                        if (locationArray[posX, posY - 1] == 0)
                        {
                            direction = DIRECTION_LEFT;
                        }
                        break;

                    case DIRECTION_DOWN:
                        posX++;
                        if (locationArray[posX, posY + 1] == 0)
                        {
                            direction = DIRECTION_RIGHT;
                        }
                        break;

                    default:
                        // just starting, set to right;
                        direction = DIRECTION_RIGHT;
                        break;
                }

                locationArray[posX, posY] = i;
            }

            var steps = Math.Abs(startingLoc - posX) + Math.Abs(startingLoc - posY);

            return steps.ToString();
        }

        public string Part2(string input)
        {
            var location = int.Parse(input);

            var sizeOfArray = 1;
            while (true)
            {
                if (sizeOfArray * sizeOfArray > location)
                {
                    break;
                }
                sizeOfArray += 2;
            }
            // for safety and not worrying about negative array checks later on
            sizeOfArray += 2;

            var locationArray = new int[sizeOfArray, sizeOfArray];

            int posX = (sizeOfArray - 1) / 2;
            int posY = (sizeOfArray - 1) / 2;

            var startingLoc = posX;

            string direction = DIRECTION_NULL;

            for (int i = 1; i <= location; i++)
            {
                switch (direction)
                {
                    case DIRECTION_RIGHT:
                        posY++;
                        if (locationArray[posX - 1, posY] == 0)
                        {
                            direction = DIRECTION_UP;
                        }
                        break;

                    case DIRECTION_LEFT:
                        posY--;
                        if (locationArray[posX + 1, posY] == 0)
                        {
                            direction = DIRECTION_DOWN;
                        }
                        break;

                    case DIRECTION_UP:
                        posX--;
                        if (locationArray[posX, posY - 1] == 0)
                        {
                            direction = DIRECTION_LEFT;
                        }
                        break;

                    case DIRECTION_DOWN:
                        posX++;
                        if (locationArray[posX, posY + 1] == 0)
                        {
                            direction = DIRECTION_RIGHT;
                        }
                        break;

                    default:
                        // just starting, set to right;
                        direction = DIRECTION_RIGHT;
                        break;
                }

                var nextValue = TotalAdjacentSquares(locationArray, posX, posY);
                locationArray[posX, posY] = nextValue;
                if (nextValue > location)
                {
                    return nextValue.ToString();
                }
            }

            return null;
        }

        private int TotalAdjacentSquares(int[,] array, int x, int y)
        {
            var total = array[x + 1, y + 1] +
                array[x + 1, y] +
                array[x + 1, y - 1] +
                array[x, y + 1] +
                array[x, y - 1] +
                array[x - 1, y + 1] +
                array[x - 1, y] +
                array[x - 1, y - 1];
            return total == 0 ? 1 : total;
        }
    }
}