﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Day7 : IDay
    {
        public string Part1(string input)
        {
            var firstParts = input.Split('\n').Where(p => p.Contains("->")).Select(p => p.Trim('\r').Split("->").First().Split(' ').First());
            var secondParts = input.Split('\n').Where(p => p.Contains("->")).Select(p => p.Trim('\r').Split("->").Last());

            var part = firstParts.Where(p => secondParts.Where(i => i.Contains(p)).Count() == 0).First();


            return part.ToString();
        }

        public string Part2(string input)
        {
            return string.Empty;
        }
    }
}