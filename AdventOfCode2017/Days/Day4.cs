﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Day4 : IDay
    {
        public string Part1(string input)
        {
            var lines = input.Split('\n');
            int validPhrases = 0;

            foreach (var line in lines)
            {
                var words = line.Trim('\r').Split(' ');
                if (words.Distinct().Count() == words.Count())
                {
                    validPhrases++;
                }
            }

            return validPhrases.ToString();
        }

        public string Part2(string input)
        {
            var lines = input.Split('\n');
            int validPhrases = 0;

            foreach (var line in lines)
            {
                var words = line.Trim('\r').Split(' ').Select(p => new String(p.ToCharArray().OrderBy(i => i).ToArray()));
                if (words.Distinct().Count() == words.Count())
                {
                    validPhrases++;
                }
            }

            return validPhrases.ToString();
        }
    }
}