﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Day6 : IDay
    {
        public string Part1(string input)
        {
            var banks = input.Split('\t').Select(p => int.Parse(p)).ToArray();
            int highestBank = 0;
            int cycles = 0;

            var knownConfigs = new List<string>();
            knownConfigs.Add(string.Join(",", banks));

            var notRepeated = true;

            do
            {
                highestBank = 0;
                for (var i = 0; i < banks.Length; i++)
                {
                    if (banks[i] > banks[highestBank])
                    {
                        highestBank = i;
                    }
                }

                var blocks = banks[highestBank];
                banks[highestBank] = 0;

                highestBank++;
                while (blocks > 0)
                {
                    if(highestBank >= banks.Length)
                    {
                        highestBank = 0;
                    }

                    banks[highestBank] = banks[highestBank] + 1;
                    highestBank++;
                    blocks--;
                }

                cycles++;
                if(knownConfigs.Contains(string.Join(",", banks)))
                {
                    notRepeated = false;
                }
                else
                {
                    knownConfigs.Add(string.Join(",", banks));
                }
            } while (notRepeated);

            return cycles.ToString();
        }

        public string Part2(string input)
        {
            var banks = input.Split('\t').Select(p => int.Parse(p)).ToArray();
            int highestBank = 0;
            int cycles = 0;

            var knownConfigs = new List<string>();
            knownConfigs.Add(string.Join(",", banks));

            var notRepeated = true;

            do
            {
                highestBank = 0;
                for (var i = 0; i < banks.Length; i++)
                {
                    if (banks[i] > banks[highestBank])
                    {
                        highestBank = i;
                    }
                }

                var blocks = banks[highestBank];
                banks[highestBank] = 0;

                highestBank++;
                while (blocks > 0)
                {
                    if (highestBank >= banks.Length)
                    {
                        highestBank = 0;
                    }

                    banks[highestBank] = banks[highestBank] + 1;
                    highestBank++;
                    blocks--;
                }

                cycles++;
                if (knownConfigs.Contains(string.Join(",", banks)))
                {
                    notRepeated = false;
                }
                else
                {
                    knownConfigs.Add(string.Join(",", banks));
                }
            } while (notRepeated);



            return (cycles - knownConfigs.IndexOf(string.Join(",", banks))).ToString();
        }
    }
}