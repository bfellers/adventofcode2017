﻿namespace AdventOfCode2017
{
    public class Day1 : IDay
    {
        public string Part1(string input)
        {
            var ch = ' ';
            int total = 0;
            foreach (var i in input)
            {
                if (ch == i)
                {
                    total += int.Parse(ch.ToString());
                }
                ch = i;
            }

            if (input[input.Length - 1] == input[0])
            {
                total += int.Parse(input[0].ToString());
            }

            return total.ToString();
        }

        public string Part2(string input)
        {
            var ch = input[0];
            int total = 0;
            for (var i = 1; i < input.Length; i++)
            {
                var halfwayLoc = (i > input.Length / 2 ? i - input.Length / 2 : i + input.Length / 2) - 1;
                if (ch == input[halfwayLoc])
                {
                    total += int.Parse(ch.ToString());
                }
                ch = input[i];
            }

            return total.ToString();
        }
    }
}