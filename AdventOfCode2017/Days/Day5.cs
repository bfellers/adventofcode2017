﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Day5 : IDay
    {
        public string Part1(string input)
        {
            var instructions = input.Split('\n').Select(p => int.Parse(p.Trim('\r'))).ToArray();
            int location = 0;
            int jumps = 0;

            while(location >= 0 && location < instructions.Count())
            {
                var nextLocation = location + instructions[location];
                instructions[location] = instructions[location] + 1;
                location = nextLocation;
                jumps++;
            }

            return jumps.ToString();
        }

        public string Part2(string input)
        {
            var instructions = input.Split('\n').Select(p => int.Parse(p.Trim('\r'))).ToArray();
            int location = 0;
            int jumps = 0;

            while (location >= 0 && location < instructions.Count())
            {
                var nextLocation = location + instructions[location];
                instructions[location] = instructions[location] + (instructions[location] >= 3 ? -1 : 1);
                location = nextLocation;
                jumps++;
            }

            return jumps.ToString();
        }
    }
}