﻿namespace AdventOfCode2017
{
    public interface IDay
    {
        string Part1(string input);
        string Part2(string input);
    }
}