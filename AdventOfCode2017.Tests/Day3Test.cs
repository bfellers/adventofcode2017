using System.IO;
using Xunit;
using Xunit.Abstractions;

namespace AdventOfCode2017.Tests
{
    public class Day3Test
    {
        private readonly ITestOutputHelper output;
        private readonly string input;
        private IDay day;

        public Day3Test(ITestOutputHelper output)
        {
            this.output = output;
            this.input = File.ReadAllText("inputs/day3.txt");
            this.day = new Day3();
        }

        [Fact]
        public void TestGetPart1Output()
        {
            var result = day.Part1(this.input);
            output.WriteLine(result);
        }

        [Fact]
        public void TestGetPart1IsValid()
        {
            var result = day.Part1(this.input);
            Assert.Equal("419", result);
        }

        [Fact]
        public void TestGetPart2Output()
        {
            var result = day.Part2(this.input);
            output.WriteLine(result);
        }

        [Fact]
        public void TestGetPart2IsValid()
        {
            var result = day.Part2(this.input);
            Assert.Equal("295229", result);
        }
    }
}