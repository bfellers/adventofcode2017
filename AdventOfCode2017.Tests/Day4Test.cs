using System.IO;
using Xunit;
using Xunit.Abstractions;

namespace AdventOfCode2017.Tests
{
    public class Day4Test
    {
        private readonly ITestOutputHelper output;
        private readonly string input;
        private IDay day;

        public Day4Test(ITestOutputHelper output)
        {
            this.output = output;
            this.input = File.ReadAllText("inputs/day4.txt");
            this.day = new Day4();
        }

        [Fact]
        public void TestGetPart1Output()
        {
            var result = day.Part1(this.input);
            output.WriteLine(result);
        }

        [Fact]
        public void TestGetPart1IsValid()
        {
            var result = day.Part1(this.input);
            Assert.Equal("325", result);
        }

        [Fact]
        public void TestGetPart2Output()
        {
            var result = day.Part2(this.input);
            output.WriteLine(result);
        }

        [Fact]
        public void TestGetPart2IsValid()
        {
            var result = day.Part2(this.input);
            Assert.Equal("119", result);
        }
    }
}